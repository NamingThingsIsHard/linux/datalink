use std::fs;
use std::fs::{create_dir, File};
use std::io::Write;
use std::path::{Path, PathBuf};

use anyhow::{anyhow, Result};
use serde::Serialize;
use serde_json;

#[derive(Serialize)]
pub struct Column {
    pub unique: bool,
    pub data_type: String,
}

#[derive(Serialize)]
pub struct Schema {
    id: Column,
    name: Column,
    age: Column,
    sex: Column,
}

pub struct Table<'a> {
    pub schema: Schema,
    // #[serde(skip)]
    pub name: &'a String,
    // #[serde(skip)]
    pub database_location: &'a Path,
    // #[serde(skip)]
    path: PathBuf,
}

pub struct Row {
    id: u8,
    name: String,
    age: u8,
    sex: char,
}

const TABLE_SCHEMA_NAME: &str = ".schema.json";

impl<'a> Table<'a> {
    fn new(database_location: &'a Path, schema: Schema, name: &'a String) -> Self {
        Table {
            schema,
            name,
            database_location: database_location.clone(),
            path: database_location.join(name),
        }
    }
    fn get_path(&self) -> PathBuf {
        return self.database_location.join(self.name);
    }
    fn create(&self) -> Result<()> {
        fs::create_dir_all(&self.path)?;

        let table_schema_path = self.path.join(TABLE_SCHEMA_NAME);
        File::create(table_schema_path)?.write(
            serde_json::to_string(&self.schema)?.as_ref()
        )?;

        return Ok(());
    }

    fn drop(&self) -> Result<()> {
        // TODO: Handle dependencies? Or is that the job of the DB engine?
        fs::remove_dir_all(&self.path)?;
        Ok(())
    }

    fn insert(&self, row: Row) -> Result<()> {
        // Basically serializing a row
        let dir = self.path.join(format!("{}", row.id));
        if dir.exists() {
            return Err(anyhow!("Row {} already exists", row.id));
        }
        create_dir(&dir)?;

        // Write fields
        File::create(dir.join("name"))?.write(row.name.as_bytes())?;
        File::create(dir.join("age"))?.write(format!("{}", row.age).as_bytes())?;
        File::create(dir.join("sex"))?.write(format!("{}", row.sex).as_bytes())?;

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use std::fs::{create_dir, create_dir_all};
    use std::path::{Path};
    use crate::{Column, Row, Schema, Table};


    #[test]
    fn write_row() {
        let root = Path::new("/dev/shm/datalink/write_row");
        create_dir_all(&root).unwrap();
        let table_name = String::from("People");
        let table = Table::new(
            &root, Schema {
                id: Column { unique: false, data_type: String::from("u8") },
                name: Column { unique: false, data_type: String::from("String") },
                age: Column { unique: false, data_type: String::from("String") },
                sex: Column { unique: false, data_type: String::from("char") },
            }, &table_name);
        table.create().unwrap();
        assert!(root.join(&table_name).exists());
        table.insert(Row {
            id: 123,
            name: "Victor Halonic".to_string(),
            age: 20,
            sex: 'A',
        }).unwrap()
    }
}
