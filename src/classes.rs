use std::any::TypeId;
use std::string::String;

macro_rules! table {
    (
        $row_name:ident,
        $table_name:ident,
        $trait_name:ident,
        $($col_name:ident, $col_type:ty),+
    ) => {

        #[derive(Debug)]
        struct $row_name {
            $(
                $col_name: $col_type,
            )+
        }
        #[derive(Debug)]
        struct $table_name {
        }
        trait $trait_name {
            fn insert_row(&self, row: $row_name);
            fn insert(&self, $($col_name: $col_type,)+){
                self.insert_row($row_name{$(
                    $col_name
                ,)+})
            }
        }
    };
}

#[cfg(test)]
mod tests {
    use std::any::TypeId;
    use std::string::String;

    #[test]
    fn setup() {
        table!(
            MonsterRow,
            MonsterTable,
            MonsterTrait,
            id, u16,
            name, String,
            size, u8
        );
        impl MonsterTrait for MonsterTable {
            fn insert_row(&self, row: MonsterRow) {
                println!("Inserting {:?}", row)
            }
        }
        let table = MonsterTable {};
        table.insert(1, String::from("something"), 10);
    }
}
